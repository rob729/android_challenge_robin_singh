package com.robin729.ixigochallenege.utils

import com.robin729.ixigochallenege.model.Airlines
import com.robin729.ixigochallenege.model.Providers
import org.junit.Assert.*
import org.junit.Test

class UtilsTest{

    @Test
    fun calculate_flight_duration(){
        val departureTime = 1396614600000
        val arrivalTime = 1396625400000
        val duration = Utils.calcDuration(arrivalTime - departureTime)
        assertEquals("3h 0min", duration)
    }

    @Test
    fun find_Airline_Name_from_code(){
        val airlines = Airlines("Spicejet", "Air India", "Go Air", "Jet Airways","Indigo")
        val name = Utils.airlineName("9W", airlines)
        assertEquals("Jet Airways", name)
    }

    @Test
    fun find_Provider_Name_from_id(){
        val airlines = Providers("MakeMyTrip", "Cleartrip", "Yatra", "Musafir")
        val name = Utils.providerName(3, airlines)
        assertEquals("Yatra", name)
    }

}