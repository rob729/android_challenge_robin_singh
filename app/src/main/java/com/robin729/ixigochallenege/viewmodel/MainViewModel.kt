package com.robin729.ixigochallenege.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.robin729.ixigochallenege.model.Info
import com.robin729.ixigochallenege.network.IxigoApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {

    private val _info = MutableLiveData<Info>()

    val info: LiveData<Info>
        get() = _info

    private val _loading = MutableLiveData<Boolean>()

    val loading: LiveData<Boolean>
        get() = _loading

    private val _infoLoadError = MutableLiveData<Boolean>()

    val infoLoadError: LiveData<Boolean>
        get() = _infoLoadError

    fun fetchRepos() {
        _loading.value = true
        CoroutineScope(Dispatchers.IO).launch {
            val request = IxigoApi().initalizeRetrofit().getInfo("5979c6731100001e039edcb3")
            withContext(Dispatchers.IO) {
                try {
                    request.enqueue(object : Callback<Info> {
                        override fun onResponse(call: Call<Info>, response: Response<Info>) {
                            _infoLoadError.value = false
                            _info.value = response.body()
                            _loading.value = false
                        }

                        override fun onFailure(call: Call<Info>, t: Throwable) {
                            _infoLoadError.value = true
                            _loading.value = false
                            Log.e("TAG", t.message)
                        }
                    })
                } catch (e: Exception) {
                    Log.e("MainActicity", "Exception ${e.message}")
                }
            }
        }
    }

}