package com.robin729.ixigochallenege.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.robin729.ixigochallenege.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
