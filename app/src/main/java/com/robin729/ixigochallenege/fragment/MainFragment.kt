package com.robin729.ixigochallenege.fragment


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.robin729.ixigochallenege.R
import com.robin729.ixigochallenege.adapter.InfoListAdapter
import com.robin729.ixigochallenege.model.FlightDetails
import com.robin729.ixigochallenege.utils.Utils
import com.robin729.ixigochallenege.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.fragment_main.*

/**
 * A simple [Fragment] subclass.
 */
class MainFragment : Fragment() {

    private val mainViewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    private val infoListAdapter: InfoListAdapter by lazy {
        InfoListAdapter()
    }

    private var list: List<FlightDetails> = ArrayList()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = infoListAdapter


        //Internet check before making api call
        if (Utils.hasNetwork(context)) {
            if ((mainViewModel.info.value == null)) {
                mainViewModel.fetchRepos()
            }
        } else {
            Snackbar
                .make(rv, "Please Check your Internet Connection", Snackbar.LENGTH_LONG)
                .show()
        }

        //Observing if data is getting load or not
        mainViewModel.loading.observe(viewLifecycleOwner, Observer {
            anim.visibility = if (it) View.VISIBLE else View.GONE
        })

        //Initialize the data to recycler adapter if change in data is observed
        mainViewModel.info.observe(viewLifecycleOwner, Observer {
            infoListAdapter.submitList(it.flights)
            infoListAdapter.appendix = it.appendix
            list = it.flights
        })

        //Observing if there is any error in fetching the data
        mainViewModel.infoLoadError.observe(viewLifecycleOwner, Observer {
            txt_error.visibility = if (it) View.VISIBLE else View.GONE
            if (it) {
                anim.visibility = View.GONE
            }
        })

        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.sort_dep -> {
                infoListAdapter.submitList(list.sortedWith(compareBy { it.departureTime }))
                infoListAdapter.notifyDataSetChanged()
                rv.scrollToPosition(0)
            }
            R.id.sort_arr -> {
                infoListAdapter.submitList(list.sortedWith(compareBy { it.arrivalTime }))
                infoListAdapter.notifyDataSetChanged()
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
