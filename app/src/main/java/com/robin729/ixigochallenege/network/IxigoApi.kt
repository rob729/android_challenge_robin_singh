package com.robin729.ixigochallenege.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class IxigoApi {
    private val BASE_URL = "http://www.mocky.io/v2/"

    fun initalizeRetrofit(): IxigoService {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
        return retrofit.create(IxigoService::class.java)
    }
}