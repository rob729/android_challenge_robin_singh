package com.robin729.ixigochallenege.network

import com.robin729.ixigochallenege.model.Info
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface IxigoService {

    @GET
    fun getInfo(@Url url: String): Call<Info>
}