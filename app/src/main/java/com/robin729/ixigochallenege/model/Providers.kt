package com.robin729.ixigochallenege.model

import com.google.gson.annotations.SerializedName

class Providers(
    @SerializedName("1") val provider1: String, @SerializedName("2") val provider2: String,
    @SerializedName("3") val provider3: String, @SerializedName("4") val provider4: String
)