package com.robin729.ixigochallenege.model

class Info (val appendix: Appendix, val flights: List<FlightDetails>)