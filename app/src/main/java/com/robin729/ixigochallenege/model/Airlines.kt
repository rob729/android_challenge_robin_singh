package com.robin729.ixigochallenege.model

import com.google.gson.annotations.SerializedName

class Airlines(
    @SerializedName("SG") val flight1: String,
    @SerializedName("AI") val flight2: String, @SerializedName("G8") val flight3: String, @SerializedName(
        "9W") val flight4: String, @SerializedName("6E") val flight5: String
)