package com.robin729.ixigochallenege.model

import com.google.gson.annotations.SerializedName

data class FlightDetails (val originCode: String, val destinationCode: String,
                          val departureTime: Long, val arrivalTime: Long,
                          val fares: List<Fares> ,val airlineCode: String,
                          @SerializedName("class") val classType: String)