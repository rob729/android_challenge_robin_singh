package com.robin729.ixigochallenege.model

data class Fares (val providerId: Int, val fare: Int)