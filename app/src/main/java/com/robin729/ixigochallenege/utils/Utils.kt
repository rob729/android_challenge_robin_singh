package com.robin729.ixigochallenege.utils

import android.content.Context
import android.net.ConnectivityManager
import com.robin729.ixigochallenege.model.Airlines
import com.robin729.ixigochallenege.model.Providers

object Utils {

    fun hasNetwork(ctx: Context?): Boolean {
        val connectivityManager =
            ctx?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun airlineName(code: String, airlines: Airlines): String{
        return when(code){
            "SG" -> { airlines.flight1 }
            "AI" -> { airlines.flight2 }
            "G8" -> { airlines.flight3 }
            "9W" -> { airlines.flight4 }
            "6E" -> { airlines.flight5 }
            else -> { airlines.flight1 }
        }
    }

    fun providerName(id:Int, providers: Providers): String{
        return when(id){
            1 -> { providers.provider1 }
            2 -> { providers.provider2 }
            3 -> { providers.provider3 }
            4 -> { providers.provider4 }
            else -> { providers.provider1 }
        }
    }

    fun calcDuration(time: Long): String{
        val minutes = (time / (1000 * 60) % 60).toInt()
        val hours = (time / (1000 * 60 * 60) % 24).toInt()
        return "${hours}h ${minutes}min"
    }
}