package com.robin729.ixigochallenege.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.robin729.ixigochallenege.databinding.PriceRowLayoutBinding
import com.robin729.ixigochallenege.model.Fares
import com.robin729.ixigochallenege.model.Providers
import com.robin729.ixigochallenege.utils.Utils

class FareListAdapter(val providers: Providers) :
    ListAdapter<Fares, FareListAdapter.ViewHolder>(FareDiffCallbacks()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = PriceRowLayoutBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, providers)
    }

    class ViewHolder(val binding: PriceRowLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Fares, providers: Providers) {
            binding.provider.text = Utils.providerName(item.providerId, providers)
            binding.fare.text = "\u20B9 ${item.fare}"
        }
    }

    class FareDiffCallbacks : DiffUtil.ItemCallback<Fares>() {
        override fun areItemsTheSame(oldItem: Fares, newItem: Fares): Boolean {
            return oldItem.providerId == newItem.providerId
        }

        override fun areContentsTheSame(oldItem: Fares, newItem: Fares): Boolean {
            return oldItem == newItem
        }
    }
}