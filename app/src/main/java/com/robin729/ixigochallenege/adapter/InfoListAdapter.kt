package com.robin729.ixigochallenege.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.robin729.ixigochallenege.databinding.InfoRowLayoutBinding
import com.robin729.ixigochallenege.model.Appendix
import com.robin729.ixigochallenege.model.FlightDetails
import com.robin729.ixigochallenege.utils.Utils
import java.text.SimpleDateFormat
import java.util.*


class InfoListAdapter :
    ListAdapter<FlightDetails, InfoListAdapter.ViewHolder>(InfoDiffCallbacks()) {

    lateinit var appendix: Appendix

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = InfoRowLayoutBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, appendix)
    }

    class ViewHolder(val binding: InfoRowLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: FlightDetails, appendix: Appendix) {
            binding.airlineName.text = Utils.airlineName(item.airlineCode, appendix.airlines)
            binding.depTime.text = getDate(item.departureTime)
            binding.arrTime.text = getDate(item.arrivalTime)
            binding.origin.text = item.originCode
            binding.destination.text = item.destinationCode
            binding.travelClass.text = "${item.classType}\nClass"
            binding.duration.text = Utils.calcDuration(item.arrivalTime - item.departureTime)
            val fareListAdapter = FareListAdapter(appendix.providers)
            binding.fareRv.adapter = fareListAdapter
            fareListAdapter.submitList(item.fares)
        }

        private fun getDate(milliSeconds: Long): String {
            val formatter = SimpleDateFormat("dd/MM/yyyy \n hh:mm a", Locale.ENGLISH)
            val calendar: Calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            return formatter.format(calendar.time)
        }
    }

    class InfoDiffCallbacks : DiffUtil.ItemCallback<FlightDetails>() {
        override fun areItemsTheSame(oldItem: FlightDetails, newItem: FlightDetails): Boolean {
            return (oldItem.arrivalTime == newItem.arrivalTime) && (oldItem.departureTime == newItem.departureTime) && (oldItem.airlineCode == newItem.airlineCode)
        }

        override fun areContentsTheSame(oldItem: FlightDetails, newItem: FlightDetails): Boolean {
            return oldItem == newItem
        }
    }

}